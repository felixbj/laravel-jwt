<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use  App\Http\Controllers\AuthController; 
use  App\Http\Controllers\Exception; 

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware' => 'api','prefix' => 'auth'], function () {
    Route::post('/signin', [AuthController::class, 'signin'])->name('auth.signin');
    Route::post('/signup', [AuthController::class, 'signup'])->name('auth.signup');
    Route::get('/authenticated', [Exception::class, 'Unauthorized'])->name('auth.authenticated');
    Route::group(['middleware' => 'auth:api'], function () {
        Route::post('/signout', [AuthController::class, 'signout'])->name('auth.signout');
        Route::post('/refresh', [AuthController::class, 'refresh'])->name('auth.refresh');
        Route::get('/user', [AuthController::class, 'thisuser'])->name('auth.thisuser');
    });
});