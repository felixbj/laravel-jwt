<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\User;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     * $this->middleware('auth:api', ['except' => ['signin', 'signup']]);
     */
    protected $exception;
    public function __construct(Exception $exception) {
        $this->exception = $exception;
    }
    /**
     * Login... Get a JWT credentials
     * @return \Illuminate\Http\JsonResponse
     */
    public function signin(Request $request){
        $validator =Validator::make($request->all(),
        [
            'email' => 'required|email',
            'password' => 'required|string|min:8',
        ]);
        if ($validator->fails())return $this->exception->BadRequestException($validator->errors());
        $credentials = $request->only(['email','password']);
        if (!$token = Auth::attempt($credentials)) return $this->exception->Unauthorized();
        return $this->exception->OK_reply('Successful login',$this->createNewToken($token));
    }
    /**
     * Register a User
     * @return \Illuminate\Http\JsonResponse
     */
    public function signup(Request $request) {
        $validator = Validator::make($request->all(),
        [
            'name' => 'required|string|between:2,100',
            'email' => 'required|string|email|max:100|unique:App\Models\User,email',
            'password' => 'required|string|min:8',//confirmed
        ]);
        if($validator->fails())return $this->exception->BadRequestException($validator->errors());//Validator::make($input, $rules, $messages); $errors = $validator->errors();
        $data = $request->only(['name','email','password']);
        try {
            $user = User::create(array_merge(
                $data,
                ['password' => bcrypt($data['password'])]
            ));
        } catch (\Illuminate\Database\QueryException $e) {
            return $this->exception->QueryException($e->errorInfo);
        }
        return $this->exception->Created($user);
    }
    /**
     * Signout user (Invalidate the token).
     * @return \Illuminate\Http\JsonResponse
     */
    public function signout() {
        Auth::logout();
        return $this->exception->OK_void('User successfully signed out');
    }
    /**
     * Refresh token.
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh() {
        if (!$token = $this->createNewToken(Auth::refresh())) return $this->exception->Unauthorized();
        return $this->exception->OK_reply('Renovated token',$token);
    }
    /**
     * Get the authenticated User.
     * @return \Illuminate\Http\JsonResponse
     */
    public function thisuser() {
        return $this->exception->OK_reply('My user',Auth::user());
    }
    /**
     * Get the token array structure.
     * @param  string $token
     * @return \Illuminate\Http\JsonResponse
     */
    protected function createNewToken($token){
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => Auth::factory()->getTTL() * 60,//cambiar
            'user' => Auth::user()
        ]);
    }
}
